export class UserDetails {
  firstName: String;
  lastName: String;
  age: Number;
  email: String;
  phone: String;
  state: String;
  country: String;
  address1: String;
  address2: String;
  companyAddress1: String;
  companyAddress2: String;
  interests: any[];
  isSubscribed: boolean;
  addressType: String;
  constructor() {}
}
