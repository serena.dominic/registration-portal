import { Injectable } from "@angular/core";

@Injectable()
export class UserService {
  public countryList: String[];
  public statesList: String[];
  constructor() {
    this.countryList = ["Afghanistan", "India", "Brazil"];
    this.statesList = ["Kerala", "Punjab", "Assam", "Delhi"];
  }
  public getCountryList(): String[] {
    return this.countryList;
  }
  public getStatesList(countryName): String[] {
    if (countryName === "Afghanistan") {
      return ["Kabul", "Kandahar", "Kunduz", "Herat", "Mazar-i-Sharif"];
    } else if (countryName === "Brazil") {
      return ["Sao Paulo", "Parana", "Amazonas", "Rio de Janeiro"];
    } else {
      return this.statesList;
    }
  }
}
