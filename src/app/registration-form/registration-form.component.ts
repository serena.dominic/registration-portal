import { Component, OnInit } from "@angular/core";
import { UserDetails } from "../shared/user-details.model";
import { FormsModule } from "@angular/forms";
import { MatSliderModule } from "@angular/material/slider";
import { UserService } from "../shared/user-service";

@Component({
  selector: "app-registration-form",
  templateUrl: "./registration-form.component.html",
  styleUrls: ["./registration-form.component.css"]
})
export class RegistrationFormComponent implements OnInit {
  url: String;
  isImageVisible: boolean;
  isHomeAddressSelected: boolean;
  isCompanyAddressSelected: boolean;
  isFormSubmitted: boolean;
  user: UserDetails = new UserDetails();
  countryList: String[];
  statesList: String[];
  interests: any[];
  userDetails: String;

  constructor(private userService: UserService) {
    this.url = "";
    this.countryList = this.userService.getCountryList();
    this.statesList = ["Please select a country first"];
    this.isImageVisible = false;
    this.isHomeAddressSelected = false;
    this.isCompanyAddressSelected = false;
    this.isFormSubmitted = false;
    this.interests = [
      { id: 0, name: "Hockey" },
      { id: 1, name: "Football" },
      { id: 2, name: "Badminton" },
      { id: 3, name: "Tennis" }
    ];
    this.user.isSubscribed = false;
    this.userDetails = "";
  }

  ngOnInit() {}

  // called during image upload
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = () => {
        // called once readAsDataURL is completed
        this.url = reader.result;
        this.isImageVisible = true;
      };
    }
  }

  // called when user selects address type as home/company
  onAddressTypeChange(event) {
    if (event.target.value === "H") {
      this.isHomeAddressSelected = true;
      this.isCompanyAddressSelected = false;
    } else {
      this.isHomeAddressSelected = false;
      this.isCompanyAddressSelected = true;
    }
  }

  // called when user selects a country
  onCountryChange(event) {
    this.statesList = this.userService.getStatesList(event.target.value);
    this.user.state = this.statesList[0];
  }

  // called when user clicks on edit profile
  onEditProfileClick() {
    this.isFormSubmitted = false;
  }

  // called when registration form submits
  onSubmit() {
    let age = "",
      interests = "";
    if (this.user.age >= 45) {
      age = "above 45";
    } else if (this.user.age >= 30) {
      age = "above 30";
    } else if (this.user.age >= 20) {
      age = "above 20";
    } else {
      age = "above 13";
    }
    if (this.user.interests.length > 0) {
      interests = "I like to play ";
      for (let i = 0; i < this.user.interests.length - 1; i++) {
        interests = interests + this.user.interests[i].name + ", ";
      }
      // If single interest, add it to string.
      if (this.user.interests.length === 1) {
        interests = interests + this.user.interests[0].name + ".";
      } else {
        interests =
          interests +
          " and " +
          this.user.interests[this.user.interests.length - 1].name +
          " .";
      }
    }
    const subscriptionMsg = this.user.isSubscribed
      ? " And please send me the news letters. "
      : "";
    this.userDetails = `I am ${this.user.firstName} ${
      this.user.lastName
    } and I am ${age} years of age and you can send your emails to ${
      this.user.email
    }. I live in the state of ${
      this.user.state
    }. ${interests}${subscriptionMsg} Please reach out to me on my phone ${
      this.user.phone
    }.`;
    this.isFormSubmitted = true;
  }

  /* Method to add tick label to the age slider. Use [displayWith]="formatLabel" in HTML. 
  Not applicable due to version compatibility issues.*/
  formatLabel(value: number | null) {
    if (value >= 45) {
      return "45&Above";
    } else if (value >= 30) {
      return "30-45";
    } else if (value >= 20) {
      return "20-29";
    } else {
      return "13-19";
    }
  }
}
